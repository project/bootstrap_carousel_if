INTRODUCTION
------------

A field formatter for the image field to add Bootstrap carousel.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/bootstrap_carousel_if
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/bootstrap_carousel_if

REQUIREMENTS
------------

 * A bootstrap theme.
 * The core file module.

INSTALLATION
------------

Enable the module.
You should now have available a Bootstrap Carousel image field formatter.

CONFIGURATION
-------------

Configuration is done via the field settings.

OVERRIDING OUTPUT
-----------------

This module uses the default `bootstrap-carousel.html.twig` template from the
Bootstrap theme. To alter the output, override that template.
